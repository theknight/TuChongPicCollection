﻿var log4js = require('log4js');
var logConfig = require("./log4js.json");
    log4js.configure(logConfig);
var logger = log4js.getLogger("default");


module.exports = logger;