﻿//初始参数设置

var comParam = {

    //设置查询方式
    "searchType": 1,//1 标签搜索 2 站点搜索

    //公共的请求参数
    reqParam: {
        "pageBegin": 1,//查询开始的页码
        "pageEnd": 2,//查询结束的页码
        "count": 20,//每页的数据条数
        "order": "weekly",//排序规则
        "before_timestamp": "",//查询时间段前
        "site_id": "469811",//站点ID（站点搜索用）
        "tag": "风光"//标签（标签搜索用）
    },

    //存储文件的路径
    "savePath": "G:\\Nodejs\\TuChongPicCollection\\TuChongPicCollection\\Files\\",

    //HTTPS请求头信息
    reqHead: {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'accept-encoding': 'gzip, deflate, sdch',
        'accept-language': 'zh-CN,zh;q=0.8',
        'cache-control': 'max-age=0',
        'cookie': 'PHPSESSID=q9mee5tfdhebnp1jj6jka21df2; webp_enabled=1; log_web_id=5001187804',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.221 Safari/537.36 SE 2.X MetaSr 1.0'
    }

};



module.exports = comParam;