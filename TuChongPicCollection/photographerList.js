﻿//获取摄影师列表
var request = require('request'),
    https = require('https'),
    params = require("./initParams.js");
    getAndSaveFile = require("./fileGetAndSave.js");
    Log = require("./Log.js");

function searchPr(url) {
    Log.info("摄影师列表请求：" + url);

    request({
        headers: params.reqHead,
        url: url,
        method: 'GET',
        gzip: true
    }, function (err, resp, body) {
        if (err) {
            Log.info("请求失败：" + url)
            console.error('[ERROR]Collection' + err);
            return;
        }

        var responseObj = JSON.parse(body);

        var user_list_obj = responseObj.data.user_list;

        for (var i = 0; i < user_list_obj.length; i++) {
            var site_id = user_list_obj[i].site_id;
            var name = user_list_obj[i].name;
            var url = user_list_obj[i].url;

            Log.info(site_id + " " + name + " " + user_list_obj[i].followers);
        }
    });
}

function searchPhotographer() {
    var pageBegin = params.reqParam.pageBegin;
    var pageEnd = params.reqParam.pageEnd;
    var count = params.reqParam.count;
    var order = params.reqParam.order;
    var before_timestamp = params.reqParam.before_timestamp;

    order = "daily-rank";

    if (pageBegin < 0) pageBegin = 1;
    if (pageEnd < pageBegin) pageEnd = pageBegin + 1;
    if (count < 0) count = 1;

    for (var i = pageBegin; i < pageEnd; i++) {
        var post_url = "https://tuchong.com/rest/users/rank?page=" + i + "&count=" + count + "&order=" + order;
        searchPr(post_url);
    }
}

module.exports = searchPhotographer;